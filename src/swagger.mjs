import * as fs from 'fs';
import * as path from 'path';
import jsYaml from 'js-yaml';
import swaggerExpress from 'swagger-ui-express';
import { URL } from 'url';
import { ROOT_URL, APP_ROOT_DIR } from './util/config';

const swaggerFile = path.resolve(APP_ROOT_DIR, 'swagger.yml');

const getSwaggerData = async () => new Promise((resolve) => {
  fs.readFile(swaggerFile, 'utf8', (err, data) => {
    const json = jsYaml.safeLoad(data);
    // override host, schemes using current data
    const u = new URL(ROOT_URL);
    json.host = u.host;
    json.schemes = [u.protocol.replace(/:/, '')];
    resolve(json);
  });
});

export default (app) => {
  app.get('/', (req, res) => {
    res.redirect(`${ROOT_URL}/swagger-ui`);
  });

  app.use(
    '/swagger-ui',
    swaggerExpress.serve,
    swaggerExpress.setup(null, {
      swaggerUrl: `${ROOT_URL}/swagger.json`,
    }),
  );

  app.get('/swagger.json', async (req, res) => {
    const swagger = await getSwaggerData();
    res.json(swagger);
  });
};
