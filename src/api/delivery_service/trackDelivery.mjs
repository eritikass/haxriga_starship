import axios from 'axios';
import { DELIVERY_API_ROOT } from '../../util/config';

export default async (purchaseId) => {
  const { data } = await axios.get(`${DELIVERY_API_ROOT}/deliveries/${purchaseId}/status`);
  return data;
};
