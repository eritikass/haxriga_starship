import axios from 'axios';
import { DELIVERY_API_ROOT } from '../../util/config';

export default async (endLat, endLong, startLat, startLong, purchaseId, transportId) => {
  const { data } = await axios.post(`${DELIVERY_API_ROOT}/deliveries`, JSON.stringify({
    deliveryType: transportId,
    end: {
      latitude: endLat,
      longitude: endLong,
    },
    orderId: purchaseId,
    start: {
      latitude: startLat,
      longitude: startLong,
    },
  }), {
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      'Access-Control-Allow-Origin': '*',
    },
  });
  return data;
};
