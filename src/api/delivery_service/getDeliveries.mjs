import axios from 'axios';
import { DELIVERY_API_ROOT } from '../../util/config';

export default async (endLat, endLong, startLat, startLong) => {
  const { data } = await axios.get(`${DELIVERY_API_ROOT}/deliveries`, {
    params: {
      endLat,
      endLong,
      startLat,
      startLong,
    },
  });
  return Object.values(data).filter(v => !!v);
};
