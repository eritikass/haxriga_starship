import { Item, Purchase } from '../util/mongoose';
import getDeliveries from './delivery_service/getDeliveries';
import createDelivery from './delivery_service/createDelivery';

const validate = (body) => {
  if (!body.latitude || !body.longitude) {
    return 'missing geo location!';
  }
  if (!body.buyerId) {
    return 'missing buyer info!';
  }
  if (!body.transportId) {
    return 'missing transportId!';
  }
  if (!body.itemId) {
    return 'item item id!';
  }
  return '';
};

export default async (req, res) => {
  const { body } = req;

  const validateError = validate(body);
  if (validateError) {
    res.status(400).send(validateError);
    return;
  }

  // TODO: validate transport id

  try {
    const i = await Item.findById(body.itemId);
    if (!i) {
      res.status(400).send('no item found with given id');
      return;
    }

    if (i.purchaseId) {
      res.status(400).send('item already purchased');
      return;
    }

    const deliveryData = await getDeliveries(
      body.latitude,
      body.longitude,
      i.latitude,
      i.longitude,
    );
    const deliver = deliveryData.find(d => d.deliveryType === body.transportId);

    if (!deliver) {
      res.status(400).send('delivery provider with given transportId not found');
      return;
    }

    const p = new Purchase(body);
    const p2 = await p.save();

    const deliveryDetails = await createDelivery(
      body.latitude,
      body.longitude,
      i.latitude,
      i.longitude,
      p2._id,
      body.transportId,
    );

    console.log('deliveryDetails', deliveryDetails);

    i.purchaseId = p2._id;
    await i.save();

    res.json({
      message: 'ok',
      purchaseId: String(i.purchaseId),
    });
  } catch (e) {
    console.log('error-purchase', e);
    res.status(400).send('error purchase item');
  }
};
