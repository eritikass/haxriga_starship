import { Item } from '../util/mongoose';
import formatItem from './util/formatItem';

export default async (req, res) => {
  const {
    latitude, longitude, max_distance: maxDistance = 3, purchased = 'hide_purchased', creator,
  } = req.query;
  if (!latitude || !longitude) {
    res.status(400).send('no geo cords given');
    return;
  }

  try {
    const filter = {};
    switch (purchased) {
      case 'show_all':
        // no filter
        break;
      case 'only_purchased':
        filter.purchaseId = {
          $exists: true,
        };
        break;
      case 'hide_purchased':
      default:
        filter.purchaseId = {
          $exists: false,
        };
    }

    if (creator) {
      filter.username = String(creator);
    }

    const items = await Item.find(filter).sort({ created: -1 }).limit(500);
    res.json(items.map(i => formatItem(i, latitude, longitude)).filter((i) => {
      if (i.distance === undefined) {
        return false;
      }
      return i.distance < Number(maxDistance);
    }));
  } catch (e) {
    console.log('e', e);
    res.status(400).send('error getting items');
  }
};
