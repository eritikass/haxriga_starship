import addItem from './addItem';
import getItem from './getItem';
import getItems from './getItems';
import purchase from './purchase';
import getItemTransports from './getItemTransports';
import purchaseTrack from './purchaseTrack';
import purchaseRate from './purchaseRate';
import getPicture from './getPicture';
import purge from './purge';

export default (app) => {
  app.post('/v1/items', addItem);
  app.get('/v1/items', getItems);
  app.get('/v1/items/:itemId', getItem);
  app.get('/v1/items/:itemId/transport', getItemTransports);
  app.post('/v1/purchase', purchase);
  app.post('/v1/purchase/:purchaseId/rate', purchaseRate);
  app.get('/v1/purchase/:purchaseId/track', purchaseTrack);

  app.purge('/v1/items', purge);

  app.get('/pictures/:type/:itemId', getPicture);
};
