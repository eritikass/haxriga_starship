import { Purchase } from '../util/mongoose';

export default async (req, res) => {
  try {
    const { purchaseId } = req.params;
    const { rate } = req.query;

    if (!purchaseId || !rate || ![1, 2, 3, 4, 5].includes(Number(rate))) {
      res.status(400).send('bad input');
      return;
    }

    const p = await Purchase.findById(purchaseId);
    if (!p) {
      res.status(400).send('no purchase found with given id');
      return;
    }

    if (p.rate) {
      res.status(400).send('purchase already rated');
      return;
    }

    p.rate = Number(rate);

    await p.save();

    res.json({
      message: 'ok',
    });
  } catch (e) {
    console.error('purchase-rate', e);
    res.status(400).send('error rating purchase');
  }
};
