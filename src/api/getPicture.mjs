import { Item } from '../util/mongoose';

import fallbackImage from './util/fallbackImage';
import fallbackAvatar from './util/fallbackAvatar';

export default async (req, res) => {
  try {
    const { itemId, type = 'items' } = req.params;
    if (!itemId) {
      res.status(400).send('no picture id given');
      return;
    }

    const key = type === 'avatars' ? 'avatars' : 'items';

    const i = await Item.findById(itemId);
    if (!i) {
      res.status(404).send('no picture found');
      return;
    }

    let imgBase64 = i[key];
    if (!imgBase64 && type === 'avatars') {
      imgBase64 = fallbackAvatar();
    } else if (!imgBase64 && type === 'items') {
      imgBase64 = fallbackImage();
    }

    const regex = /^data:image\/(?<type>.+);base64,(?<base64>.+)$/;
    const match = regex.exec(imgBase64.trim());
    if (!match) {
      res.status(400).send('bad picture found');
      return;
    }

    const img = Buffer.from(match.groups.base64, 'base64');

    res.writeHead(200, {
      'Content-Type': `image/${match.groups.type}`,
    });
    res.end(img);
  } catch (e) {
    res.status(400).send('error getting picture');
  }
};
