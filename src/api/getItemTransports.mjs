import { Item } from '../util/mongoose';
import getDeliveries from './delivery_service/getDeliveries';

export default async (req, res) => {
  const { itemId } = req.params;
  const { latitude, longitude } = req.query;

  try {
    if (!latitude || !longitude) {
      res.status(400).send('missing geo location!');
      return;
    }

    const i = await Item.findById(itemId);
    if (!i) {
      res.status(404).send('no item found');
      return;
    }

    if (i.purchaseId) {
      res.status(400).send('item already purchased');
      return;
    }

    const deliveryData = await getDeliveries(latitude, longitude, i.latitude, i.longitude);

    res.json(Object.values(deliveryData).map(d => ({
      transportId: d.deliveryType,
      usable: true,
      eta: d.eta,
      price: d.price,
    })));
  } catch (e) {
    console.error('transports', e);
    res.status(400).send('error getting transport options');
  }

  res.send('ok');
};
