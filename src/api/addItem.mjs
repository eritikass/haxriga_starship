import { Item } from '../util/mongoose';

import formatItem from './util/formatItem';
import fallbackImage from './util/fallbackImage';
import fallbackAvatar from './util/fallbackAvatar';

const validate = (body) => {
  if (!body.latitude || !body.longitude) {
    return 'missing geo location!';
  }
  if (!body.price) {
    return 'missing price!';
  }
  if (!body.picture) {
    return 'missing picture!';
  }
  if (!body.itemname) {
    return 'item name missing!';
  }
  if (body.itemdescr === undefined) {
    return 'item description missing!';
  }
  if (!body.username) {
    return 'username missing!';
  }
  if (!body.useravatar) {
    return 'missing user picture!';
  }
  return '';
};

export default async (req, res) => {
  const { body } = req;

  if (!body.picture) {
    body.picture = fallbackImage();
  }

  if (!body.useravatar) {
    body.useravatar = fallbackAvatar();
  }

  const validateError = validate(body);
  if (validateError) {
    res.status(400).send(validateError);
    return;
  }

  body.created = new Date();

  const item = new Item(body);

  const savedItem = await item.save();
  res.json(formatItem(savedItem));
};
