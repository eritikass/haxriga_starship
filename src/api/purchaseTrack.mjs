import { Purchase } from '../util/mongoose';

import trackDelivery from './delivery_service/trackDelivery';

export default async (req, res) => {
  try {
    const { purchaseId } = req.params;

    console.log('purchaseId', purchaseId);

    if (!purchaseId) {
      res.status(400).send('bad input');
      return;
    }

    const p = await Purchase.findById(purchaseId);
    if (!p) {
      res.status(400).send('no purchase found with given id');
      return;
    }

    const { deliveryStatusDto = {} } = await trackDelivery(purchaseId);

    console.log('deliveryStatusDto', deliveryStatusDto);

    res.json({
      itemId: p.itemId,
      status: String(deliveryStatusDto.deliveryStatus || ''),
      eta: String(deliveryStatusDto.eta || ''),
      transportId: p.transportId,
    });
  } catch (e) {
    console.error('purchase-track', e);
    res.status(400).send('error track purchase');
  }
};
