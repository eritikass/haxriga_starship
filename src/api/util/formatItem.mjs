/* eslint-disable no-underscore-dangle */
import turf from '@turf/turf';

import { ROOT_URL } from '../../util/config';

const getPictureUrl = (id, imgVal, type) => {
  if (typeof imgVal === 'string' && imgVal.substring(0, 7) === 'static:') {
    return `${ROOT_URL}/static/${imgVal.substring(7)}`;
  }
  return `${ROOT_URL}/pictures/${type}/${id}`;
};

export default (dbItem, latitude = null, longitude = null) => {
  const i = {
    itemId: dbItem._id,
    latitude: dbItem.latitude,
    longitude: dbItem.longitude,
    price: dbItem.price,
    pictureUrl: getPictureUrl(dbItem._id, dbItem.picture, 'pictures'),
    itemname: dbItem.itemname,
    itemdescr: dbItem.itemdescr,
    username: dbItem.username,
    useravatarUrl: getPictureUrl(dbItem._id, dbItem.useravatar, 'avatars'),
    created: dbItem.created ? dbItem.created.toISOString() : null,
  };
  if (latitude && longitude) {
    const from = turf.point([Number(i.longitude), Number(i.latitude)]);
    const to = turf.point([Number(longitude), Number(latitude)]);
    i.distance = turf.distance(from, to, { units: 'kilometers' });
  }
  if (dbItem.purchaseId) {
    i.purchaseId = dbItem.purchaseId;
  }
  return i;
};
