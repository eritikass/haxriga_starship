import { Purchase, Item } from '../util/mongoose';

export default async (req, res) => {
  try {
    await Item.deleteMany({});
    await Purchase.deleteMany({});
    res.send('ok');
  } catch (e) {
    console.error('purge-error', e);
    res.status(400).send('error purge');
  }
}
