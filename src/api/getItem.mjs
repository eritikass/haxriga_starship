import { Item } from '../util/mongoose';

import formatItem from './util/formatItem';

export default async (req, res) => {
  try {
    const { itemId } = req.params;
    if (!itemId) {
      res.status(400).send('no item id given');
      return;
    }

    const i = await Item.findById(itemId);
    if (!i) {
      res.status(400).send('no item found with given id');
      return;
    }

    res.json(formatItem(i));
  } catch (e) {
    res.status(400).send('error getting item');
  }
};
