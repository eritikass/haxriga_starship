import mongoose from 'mongoose';

import { MONGO_URL } from './config';

export const connectToMongo = () => {
  mongoose.connect(MONGO_URL, { useNewUrlParser: true });
};
mongoose.connection.on('connecting', () => {
  console.log("connecting to MongoDB (url: '%s')", MONGO_URL);
});
mongoose.connection.on('disconnected', () => {
  console.log('MongoDB disconnected!');
  connectToMongo();
});

// models definitions

export const Purchase = mongoose.model('Purchase', {
  itemId: String,
  buyerId: String,
  transportId: String,
  latitude: Number,
  longitude: Number,
  rate: Number,
});

export const Item = mongoose.model('Item', {
  latitude: Number,
  longitude: Number,
  price: Number,
  picture: String,
  itemname: String,
  itemdescr: String,
  username: String,
  useravatar: String,
  created: Date,
  purchaseId: String,
});
