import * as path from 'path';

export const { ROOT_URL } = process.env;

export const { MONGO_URL } = process.env;

export const { PORT = 3000 } = process.env;

export const HTTP_MAX_POST_SIZE = '3mb';

const dirname = path.dirname(new URL(import.meta.url).pathname);
export const APP_ROOT_DIR = path.resolve(dirname, '../..');

export const DELIVERY_API_ROOT = 'http://ec2-54-162-78-81.compute-1.amazonaws.com';
