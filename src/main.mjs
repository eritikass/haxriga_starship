import http from 'http';
import * as path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import terminus from '@godaddy/terminus';
import cors from 'cors';

import swagger from './swagger';
import api from './api';

import { ROOT_URL, HTTP_MAX_POST_SIZE, PORT, APP_ROOT_DIR } from './util/config';
import { connectToMongo } from './util/mongoose';

// open mongo connection
connectToMongo();

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ limit: HTTP_MAX_POST_SIZE, extended: false }));
app.use(bodyParser.json({ limit: HTTP_MAX_POST_SIZE }));

app.use('/static', express.static(path.join(APP_ROOT_DIR, 'static')));

app.use((req, res, next) => {
  // https://stackoverflow.com/questions/20429592/no-cache-in-a-nodejs-server
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
});

swagger(app);
api(app);

const server = http.createServer(app);

terminus.createTerminus(server, {
  timeout: 50,
  onShutdown: async () => {
    console.log('pod is shutting down');
  },
});

server.listen(PORT, () => {
  console.log(`Server is now running on ${ROOT_URL}/swagger-ui`);
});
