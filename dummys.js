const axios = require('axios');

const eldar = {
  username: 'Eldar',
  avatar: 'photo_2019-02-21_08.42.16.jpeg',
};

const user1 = {
  username: 'henx',
  avatar: 'avatar-I80W1Q0.png'
};

const user2 = {
  username: 'cat1',
  avatar: 'lolcats.jpeg'
};

const user3 = {
  username: 'driver',
  avatar: 'car1.jpeg'
};

const items = [
  {
    name: 'Wooden stick figure',
    description: 'Classical Latvian children toy made from wood. Works also as decorative item.',
    price: 4,
    image: 'IMG_3230.jpg',
    ...eldar,
  },
  {
    name: 'yellow raincoat',
    price: 25,
    image: 'jane-post-yellow-raincoat-102933789_vert.jpg?v=2',
    ...user3,
  },
  {
    name: 'Red leather handbag',
    price: 29,
    image: '46854bcb46ea8e0d7aeaf1964b468b63.jpg',
    ...user2,
  },
  {
    name: 'Unicorn pool toy',
    price: 19,
    image: 'giant-inflatable-pool-toy-unicorn-floats.jpg',
    ...user1,
  },
  {
    name: 'New iPhone 8',
    price: 578,
    image: 'for-sale-apple-iphone-x-500x500.jpg',
    ...user3,
  },
  {
    name: 'Mega saw',
    price: 233,
    image: 'Makita-Dual-18v-Tools1-1024x702.jpg',
    ...user2,
  },
  {
    name: 'Inferno by Dan Brown',
    price: 20,
    image: 'inferno.jpg',
    ...user1,
  },
  {
    name: 'Playstation 4 with a remote',
    price: 399,
    image: 'dims.jpeg',
    ...user3,
  },
];

const { ROOT_URL } = process.env;


async function upload() {
  if (!ROOT_URL) {
    console.log(`
ERROR: ROOT_URL env value not defined!

usage:
ROOT_URL=https://haxriga.kassikas.com node dummys.js   (live)
ROOT_URL=http://localhost:3000 node dummys.js          (dev)

if you want to clean db
curl -X PURGE http://localhost:3000/v1/items
curl -X PURGE https://haxriga.kassikas.com/v1/items
`);
    return;
  }

  const apiUrl = `${ROOT_URL}/v1/items`;
  const settings = {
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    },
  };

  await Promise.all(items.reverse().map(async (item) => {
    const data = {
      latitude: item.latitude || 56.9750744,
      longitude: item.longitude || 24.1508032,
      price: item.price,
      picture: `static:${item.image}`,
      itemname: item.name,
      itemdescr: item.description || '',
      username: item.username || 'triinu',
      useravatar: item.avatar ? `static:${item.avatar}` : '',
    };
    console.log('addItem', data);
    await axios.post(apiUrl, JSON.stringify(data), settings).catch(err => {
      console.error('create-error', data, err);
    });
  }));
}
upload();
